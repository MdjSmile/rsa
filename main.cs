using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Text;

namespace RSA
{
    class Program
    {
	    static long d, n;
		static string someText = "Вот так напимер";
		static char[] characters = { '#', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И',
		    'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С',
		    'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ь', 'Ы', 'Ъ',
		    'Э', 'Ю', 'Я', ' ', '1', '2', '3', '4', '5', '6', '7',
		    '8', '9', '0' };

		static void Main(string[] args)
        {
			buttonEncrypt_Click();
	        buttonDecipher_Click();
	        //Console.ReadLine();
        }

	    private static void buttonEncrypt_Click()
	    {
			long p = 53;
			long q = 59;

			if (IsTheNumberSimple(p) && IsTheNumberSimple(q))
			{ 
				someText = someText.ToUpper();

				n = p * q;									// open
				long m = (p - 1) * (q - 1);

				d = Calculate_d(m);							// safe
				long e_ = Calculate_e(d, m);				// open
				//long e_ = 3;
				List<string> result = RSA_Encode(someText, e_, n);

				StreamWriter sw = new StreamWriter("out1.txt");
				foreach (string item in result)
					sw.WriteLine(item);
				sw.Close();
			}
			else
				Console.WriteLine("p или q - не простые числа!");
	    }

	    private static void buttonDecipher_Click()
	    {
			List<string> input = new List<string>();

			StreamReader sr = new StreamReader("out1.txt");

			while (!sr.EndOfStream)
			{
				input.Add(sr.ReadLine());
			}

			sr.Close();

			string result = RSA_Decode(input, d, n);

			Console.WriteLine(result);
			
	    }

	   
	    private static List<string> RSA_Encode(string s, long e, long n)				// client encode
	    {
		    List<string> result = new List<string>();
			StringBuilder sb = new StringBuilder();

		    BigInteger bi;

		    for (int i = 0; i < s.Length; i++)
		    {
			    int index = Array.IndexOf(characters, s[i]);

			    bi = new BigInteger(index);
			    bi = BigInteger.Pow(bi, (int)e);

			    BigInteger n_ = new BigInteger((int)n);

			    bi = bi % n_;

			    result.Add(bi.ToString());
			    sb.Append(bi + " ");
		    }


		    return result;
	    }

	    private static string RSA_Decode(List<string> input, long d, long n)		// server decode
	    {
		    string result = "";

		    BigInteger bi;

		    foreach (string item in input)
		    {
			    bi = new BigInteger(Convert.ToDouble(item));
			    bi = BigInteger.Pow(bi, (int)d);

			    BigInteger n_ = new BigInteger((int)n);

			    bi = bi % n_;

			    int index = Convert.ToInt32(bi.ToString());

			    result += characters[index].ToString();
		    }

		    return result;
	    }

	    private static bool IsTheNumberSimple(long n)
	    {
		    if (n < 2)
			    return false;

		    if (n == 2)
			    return true;

		    for (long i = 2; i < n; i++)
			    if (n % i == 0)
				    return false;

		    return true;
	    }

		private static long Calculate_d(long m)
	    {
		    long d = m - 1;

		    for (long i = 2; i <= m; i++)
			    if ((m % i == 0) && (d % i == 0)) //если имеют общие делители
			    {
				    d--;
				    i = 1;
			    }

		    return d;
	    }

	    private static long Calculate_e(long d, long m)
	    {
		    long e = 10;

		    while (true)
		    {
			    if ((e * d) % m == 1)
				    break;
				e++;
		    }

		    return e;
	    }
	}
}
